from django.db import models
from client.models import Client

# Create your models here.

class Bill(models.Model):
    clien_id = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name="cliente", related_name="get_client") 
    nit = models.CharField(max_length=15, verbose_name="Nit")
    address = models.CharField(max_length=60, verbose_name="Dirección")

    class Meta: 
        verbose_name = "Cuenta"
        verbose_name_plural = "Cuentas" 

    def __str__(self):
        return self.clien_id.last_name + self.nit
    
class Product(models.Model):
    name = models.CharField(max_length=50, verbose_name="Nombre")
    description = models.TextField(max_length=800, verbose_name="Descripción")

    class Meta: 
        verbose_name = "Producto"
        verbose_name_plural = "Productos" 

    def __str__(self):
        return self.name

class Products_Bill(models.Model):
    bill = models.ForeignKey(Bill, on_delete=models.CASCADE, verbose_name="Cuenta")
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name="Producto")
    quantity = models.IntegerField(verbose_name="Cantidad")
    class Meta:
        verbose_name = "Producto de la cuenta"
        verbose_name_plural = "Productos de la cuenta"
