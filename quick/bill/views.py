from django.shortcuts import render
from rest_framework import viewsets
from .serializers import BillSerializer, Bill_Serializer, ProductBillSerializer, ProductSerializer
from .models import Bill, Product, Products_Bill

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser

class BillViewSet(viewsets.ModelViewSet):
    """
    Listar clientes
    """
    queryset = Bill.objects.all()
    serializer_class = BillSerializer

@csrf_exempt
def billList(request):
    """
    Listar, y crear a nueva cuenta.
    """
    if request.method == 'GET':
        bills = Bill.objects.all()
        serializer = Bill_Serializer(bills)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = Bill_Serializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

class ProductViewSet(viewsets.ModelViewSet):
    """
    Listar productos
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductsBillViewSet(viewsets.ModelViewSet):
    """
    Listar clientes
    """
    queryset = Products_Bill.objects.all()
    serializer_class = ProductBillSerializer
