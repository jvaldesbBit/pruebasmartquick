from django.contrib import admin
from .models import Bill, Product, Products_Bill

# Register your models here.
admin.site.register(Bill)
admin.site.register(Product)
admin.site.register(Products_Bill)