from rest_framework import serializers
from .models import Bill, Products_Bill, Product

class BillSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Bill
        fields = ['id', 'clien_id', 'nit', 'address']

class Bill_Serializer(serializers.Serializer):
    clien_id = serializers.CharField(read_only=True, max_length=15)
    nit = serializers.IntegerField(read_only=True)
    address = serializers.CharField(max_length=60)

    def create(self, validated_data):
        """
        Crear cuenta
        """
        return Bill.objects.create(**validated_data)
    
    def update(self, instance, validated_data):
        """
        Actualizar cuenta.
        """
        instance.nit = validated_data.get('nit', instance.nit)
        instance.address = validated_data.get('address', instance.address)
        instance.save()
        return instance


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'name', 'description']

class Product_Serializer(serializers.Serializer):
    clien_id = serializers.CharField(read_only=True, max_length=15)
    nit = serializers.IntegerField(read_only=True)
    address = serializers.CharField(max_length=60)

    def create(self, validated_data):
        """
        Crear cuenta
        """
        return Bill.objects.create(**validated_data)
    
    def update(self, instance, validated_data):
        """
        Actualizar cuenta.
        """
        instance.nit = validated_data.get('nit', instance.nit)
        instance.address = validated_data.get('address', instance.address)
        instance.save()
        return instance


class ProductBillSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Products_Bill
        fields = ['id', 'bill', 'product', 'quantity']