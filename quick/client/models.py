from django.db import models

# Create your models here.

class Client(models.Model):
    clien_id = models.CharField(primary_key=True, max_length=15, verbose_name="cliente Id") 
    last_name = models.CharField(max_length=50, verbose_name="Nombre (s)")
    first_name = models.CharField(max_length=50, verbose_name="Apellido (s)")
    address = models.CharField(max_length=60, verbose_name="Dirección")

    class Meta: 
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes" 

    def __str__(self):
        return self.first_name + self.last_name
