from django.shortcuts import render
from rest_framework import viewsets
from .serializers import ClientSerializer, Clien_Serializer
from .models import Client

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser

# Create your views here.

class ClientViewSet(viewsets.ModelViewSet):
    """
    Listar clientes
    """
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


@csrf_exempt
def cleintList(request):
    """
    Listar, y crear a nuevo cliente.
    """
    if request.method == 'GET':
        clients = Client.objects.all()
        serializer = Clien_Serializer(clients)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = Clien_Serializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)