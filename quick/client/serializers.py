from rest_framework import serializers
from .models import Client

class ClientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ['clien_id', 'last_name', 'first_name', 'address']

class Clien_Serializer(serializers.Serializer):
    clien_id = serializers.CharField(read_only=True, max_length=15)
    last_name = serializers.CharField(max_length=50)
    first_name = serializers.CharField(max_length=50)
    address = serializers.CharField(max_length=60)

    def create(self, validated_data):
        """
        Crear cliente
        """
        return Client.objects.create(**validated_data)
    
    def update(self, instance, validated_data):
        """
        Actualizar cliente.
        """
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.address = validated_data.get('address', instance.address)
        instance.save()
        return instance


